# Packages

Packages registry

## Conan packages

Remote:
```shell
conan remote add gitlab https://gitlab.com/api/v4/projects/25668674/packages/conan
```

The Conan packages are built using specific profiles and settings.
See the [conan-config](https://gitlab.com/scandyna/conan-config) repository for more informations.

Packages are build from other projects, tested,
then uploaded to the [registry of this project](https://gitlab.com/scandyna/packages/-/packages).

Projects that pushes packages here:
- [conan-qt-builds](https://gitlab.com/scandyna/conan-qt-builds)
- [conan-packages-builds](https://gitlab.com/scandyna/conan-packages-builds)
